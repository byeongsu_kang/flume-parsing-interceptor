package com.toss.flume.google;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GoogleSpreadsheet {
    private static final Logger logger = LoggerFactory
            .getLogger(GoogleSpreadsheet.class);

    private JSONArray metaCache = new JSONArray();
    private JSONArray schemaCache = new JSONArray();

    /**
     * A : Source Column
     * B : Source Type
     * C : Target Column
     * D : Target Type
     * E : Target Example
     * F : Convert Type
     * G : param1
     * H : param2
     * J2 : Source Format
     * J3 : Target Format
     * J4 : Kafka topic
     * J5 : HDFS Path
     * J6 : Hive Table
     * J7 : Parsing 에러시 데이터 버릴지?
     */
    public boolean getSheetData(String spreadsheetId, String sheetName, String url) {
        try {

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet getRequest = new HttpGet(
                    url + "/" + spreadsheetId + "/" + sheetName);
            getRequest.addHeader("accept", "application/json");

            logger.info("Request URL : " + getRequest.getURI());
            HttpResponse response = httpClient.execute(getRequest);

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatusLine().getStatusCode());
            }
            BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));

            String output;
            StringBuilder res = new StringBuilder();
            while ((output = br.readLine()) != null) {
                res.append(output);
            }

            JSONArray arr = new JSONArray(res.toString());

            httpClient.getConnectionManager().shutdown();
            caching(arr);
            return true;
        } catch (IOException ioe) {
            logger.error("spreadsheet request error. " + ioe);
            return false;
        }
    }

    public void caching(JSONArray values) {

        int metaIndex = 0;
        int schemaIndex = 0;
        for (int i = 0; i < values.length(); i++) {
            JSONArray row = (JSONArray) values.get(i);
            if (row.length() == 0) {
                logger.error("Row Empty.");
            } else {
                if (row.get(0).equals("META")) {
                    metaIndex = i;
                } else if (row.get(0).equals("SCHEMA")) {
                    schemaIndex = i;
                }
            }
        }
        for (int i = metaIndex + 1; i < schemaIndex; i++) {
            metaCache.put(values.get(i));
        }
        for (int i = schemaIndex + 2; i < values.length(); i++) {
            schemaCache.put(values.get(i));
        }
    }

    public Map<String, String> loadParseLogic() {
        if (schemaCache == null || schemaCache.length() == 0) {
            logger.error("No data found.");
            return new HashMap<>();
        } else {
            Map<String, String> parseLogic = new HashMap<>();
            for (Object obj : schemaCache) {
                JSONArray row = (JSONArray) obj;
                if (row.length() == 0) {
                    logger.error("Row Empty.");
                } else {
                    logger.info(row.toString());
                    if (!row.get(2).equals("")) {
                        String columnName = row.get(0).toString();
                        logger.info(columnName);
                        String logic = row.get(2) + "|" + row.get(3) + "|" + row.get(4);
                        parseLogic.put(columnName, logic);
                    }
                }
            }
            return parseLogic;
        }
    }

    public String getSourceFormat() {
        JSONArray row = (JSONArray) metaCache.get(0);
        return row.get(1).toString();
    }

    public String getTargetFormat() {
        JSONArray row = (JSONArray) metaCache.get(1);
        return row.get(1).toString();
    }

    public List<String> arrangeColumnName() {
        if (schemaCache == null || schemaCache.length() == 0) {
            logger.error("No data found.");
            return new ArrayList<>();
        } else {
            List<String> columns = new ArrayList<>();
            for (Object obj : schemaCache) {
                JSONArray row = (JSONArray) obj;
                if (row.length() == 0) {
                    logger.error("Row Empty.");
                } else {
                    columns.add(row.get(0).toString());
                }
            }
            return columns;
        }
    }

    public boolean dumpData() {
        if (metaCache == null || metaCache.length() == 0) {
            logger.error("Dump data : No data found.");
            return false;
        } else {
            JSONArray row = (JSONArray) metaCache.get(5);
            if (row.length() == 0) {
                logger.error("Row Empty.");
            } else {
                if (row.get(1).toString().toLowerCase().equals("true")) {
                    return true;
                } else {
                    return false;
                }
            }
            return false;
        }
    }

   /* public static void main(String[] args) throws IOException {
        //GET parsing logic
        System.out.println("start");
        String spreadsheetId = "1kdRbhGDIOMgInJiY0xG6xuAH7dO9yxbODbjr5VlLF_I";
        String sheetname = "push-log";
        GoogleSpreadsheet googleSpreadsheet = new GoogleSpreadsheet();
        boolean spread = googleSpreadsheet.getSheetData(spreadsheetId, sheetname, StringConstans.SPREADSHEET_URL);

        String sf = googleSpreadsheet.getSourceFormat();
        String tf = googleSpreadsheet.getTargetFormat();
        boolean isDP = googleSpreadsheet.dumpData();
        Map<String, String> par = googleSpreadsheet.loadParseLogic();
        List<String> cols = googleSpreadsheet.arrangeColumnName();

        System.out.println(sf + " , " + tf + " , " + isDP);
        System.out.println(" -------- parse logic -----------");
        for (String key : par.keySet()) {
            System.out.println(key + " , " + par.get(key));
        }
        System.out.println(" -------- cols -----------");
        for (String key : cols) {
            System.out.println(key);
        }

    }*/
}
