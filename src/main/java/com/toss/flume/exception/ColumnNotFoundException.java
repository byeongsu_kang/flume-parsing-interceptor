package com.toss.flume.exception;

public class ColumnNotFoundException extends RuntimeException{

    public ColumnNotFoundException(){
        super();
    }
    public ColumnNotFoundException(String message){
        super(message);
    }
}
