package com.toss.flume.parser;

import com.toss.flume.utils.StringConstans;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Map;

public abstract class Parser {
    private String targetDelimeter;
    private List<String> columns;
    private Map<String, String> parsingLogic;
    private static final Logger logger = LoggerFactory
            .getLogger(Parser.class);

    public Parser(String targetDelimeter, List<String> columns, Map<String, String> parsingLogic) {
        this.targetDelimeter = targetDelimeter;
        this.columns = columns;

        this.parsingLogic = parsingLogic;
    }

    abstract public JSONObject parse(String payload);

    /**
     * make target delimited string
     * and apply parsing logic
     **/
    public String logic(JSONObject kvs) {
        StringBuilder result = new StringBuilder();
        try {
            for (int i = 0; i < columns.size(); i++) {
                String key = columns.get(i);

                if (kvs.has(key) && kvs.get(key) != null) {
                    Object columnValue = kvs.get(key);
                    if (parsingLogic.containsKey(key)) {
                        if (columnValue == null) {
                            result.append(" ");
                        } else {
                            String[] splited = parsingLogic.get(key).split("\\|");
                            String type = splited[0];
                            String val = columnValue.toString();
                            switch (type) {
                                case "substr":
                                    int from = Integer.parseInt(splited[1]);
                                    int to = Integer.parseInt(splited[2]);
                                    String after;
                                    if (val.length() >= (to - from)) {
                                        after = val.substring(from, to);
                                    } else {
                                        after = val;
                                    }
                                    result.append(after);
                                    break;
                                case "parse":
                                    String source = splited[1];
                                    String target = splited[2];


                                    if (source.equals("string")) {
                                        String stringVal = columnValue.toString();
                                        if (target.equals("int")) {
                                            result.append(Integer.parseInt(stringVal));
                                        } else if (target.equals("long")) {
                                            result.append(Long.parseLong(stringVal));
                                        } else if (target.equals("boolean")) {
                                            result.append(Boolean.parseBoolean(stringVal));
                                        } else if (target.equals("float")) {
                                            result.append(Float.parseFloat(stringVal));
                                        }
                                    } else {
                                        result.append(val);
                                    }
                                    break;
                                case "date":
                                    try {
                                        DateTimeFormatter sourceFormat = DateTimeFormatter.ofPattern(splited[1]);
                                        DateTimeFormatter targetFormat = DateTimeFormatter.ofPattern(splited[2]);
                                        LocalDateTime dateTime = LocalDateTime.parse(val, sourceFormat);
                                        String newDate = dateTime.format(targetFormat);
                                        result.append(newDate);
                                    } catch (DateTimeParseException dpe) {
                                        logger.error("Date parsing error.\n" + dpe + "\n 들어온 값 : " + val);
                                        return "";
                                    }
                                    break;
                            }
                        }
                    } else {
                        result.append(columnValue);
                    }
                } else {
                    result.append(StringConstans.EMPTY_STRING);
//                    throw new ColumnNotFoundException(key + " Column Not Found in data.");
//                    logger.error(key + " Column Not Found in data.");
                }
                if (i < (columns.size() - 1)) {
                    result.append(targetDelimeter);
                }
            }
            return result.toString();
        } catch (Exception e) {
            logger.error("Parsing Logic 수행중에 에러 발생." + e);
            return "";
        }
    }
}
