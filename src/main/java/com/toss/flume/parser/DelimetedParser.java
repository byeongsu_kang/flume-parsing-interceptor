package com.toss.flume.parser;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DelimetedParser extends Parser {
    private static final Logger logger = LoggerFactory
            .getLogger(DelimetedParser.class);

    private String sourceDelimiter;
    private List<String> columns;

    public DelimetedParser(String sourceDelimiter, String targetDelimiter, List<String> columns, Map<String, String> parsingLogic) {
        super(targetDelimiter, columns, parsingLogic);
        this.sourceDelimiter = sourceDelimiter;
        this.columns = columns;
    }

    @Override
    public JSONObject parse(String payload) {
        JSONObject jsonObject=new JSONObject();
        String[] splited = payload.split(sourceDelimiter);
        if (columns.size() != splited.length) {
            logger.error("정의된 로그 스키마와 실제 데이터가 일치하지 않습니다." + payload);
            return jsonObject;
        }

        for (int i = 0; i < columns.size(); i++) {
            jsonObject.put(columns.get(i), splited[i]);
        }
        return jsonObject;
    }
}