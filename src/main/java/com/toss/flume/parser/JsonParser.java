package com.toss.flume.parser;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public class JsonParser extends Parser {
    //    private final ObjectMapper mapper;
    private static final Logger logger = LoggerFactory
            .getLogger(JsonParser.class);


    public JsonParser(String targetDelimiter, List<String> columns, Map<String, String> parsingLogic) {
        super(targetDelimiter, columns, parsingLogic);
//        this.mapper = new ObjectMapper();
    }

    @Override
    public JSONObject parse(String payload) {
        return parseJson(payload);
    }


    private JSONObject parseJson(String payload) {
        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(payload);
        } catch (Exception e) {
            logger.error("Error in Json Parsing : " + e);
        }
        return jsonObj;
    }
}