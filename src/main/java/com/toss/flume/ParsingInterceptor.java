package com.toss.flume;

import com.toss.flume.google.GoogleSpreadsheet;
import com.toss.flume.parser.DelimetedParser;
import com.toss.flume.parser.JsonParser;
import com.toss.flume.parser.Parser;
import com.toss.flume.utils.StringConstans;
import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.interceptor.Interceptor;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ParsingInterceptor implements Interceptor {
    private static final Logger logger = LoggerFactory
            .getLogger(ParsingInterceptor.class);

    private String spreadsheetID;
    private String sheetName;
    private String timeColumn;
    private boolean useEventtime;
    private final DateTimeFormatter dateTimeFormatter;
    private final GoogleSpreadsheet googleSpreadsheet = new GoogleSpreadsheet();

    private Parser parser;

    private ParsingInterceptor(String spreadsheetID, String sheetName, boolean useEventtime, String timeColumn, String timeFormat) {
        this.spreadsheetID = spreadsheetID;
        this.sheetName = sheetName;
        this.useEventtime = useEventtime;
        this.timeColumn = timeColumn;
        this.dateTimeFormatter = DateTimeFormatter.ofPattern(timeFormat);
    }

    @Override
    public void initialize() {
        logger.info("Initialize");
        loadMetadata();
    }

    public boolean dumpData() {
        boolean dump = true;
//        boolean dump = googleSpreadsheet.getSheetData(spreadsheetID, sheetName, StringConstans.SPREADSHEET_URL);
        if (dump) {
            boolean isDump = googleSpreadsheet.dumpData();
            logger.info("get dump data value : " + isDump);
            return isDump;
        } else {
            return false;
        }
    }

    public void loadMetadata() {
        logger.info("Load Metadata.");
        logger.info("Sheer ID : " + spreadsheetID + " Sheet name : " + sheetName);

        boolean spread = googleSpreadsheet.getSheetData(spreadsheetID, sheetName, StringConstans.SPREADSHEET_URL);
        if (spread) {
            List<String> columns = googleSpreadsheet.arrangeColumnName();
            Map<String, String> parsingLogic = googleSpreadsheet.loadParseLogic();
            String sourceFormat = googleSpreadsheet.getSourceFormat();
            String targetFormat = googleSpreadsheet.getTargetFormat();

            String targetDelimiter = getDelimiter(targetFormat);


            if (sourceFormat.equals(StringConstans.JSON)) {
                parser = new JsonParser(targetDelimiter, columns, parsingLogic);
            } else {
                String sourceDelimiter = getDelimiter(sourceFormat);
                parser = new DelimetedParser(sourceDelimiter, targetDelimiter, columns, parsingLogic);
            }

            logger.info("Parsing Logic!");
            for (String key : parsingLogic.keySet()) {
                logger.info(key + "-" + parsingLogic.get(key));
            }
            logger.info("Columns!");
            for (String col : columns) {
                logger.info("Column : " + col);
            }
        } else {
            logger.error("Fail getting schema by keeper of the schema");
        }
    }

    public String getMillisByEventtime(String eventTimeString) {
        // Set Event Time with source format
        try {
            LocalDateTime eventTime = LocalDateTime.parse(eventTimeString, dateTimeFormatter);
            ZonedDateTime zdt = eventTime.atZone(ZoneId.systemDefault());
            return Long.toString(zdt.toInstant().toEpochMilli());
        } catch (Exception e) {
            logger.error("Error occur in middle of EventTime parsing. " + e + "\n" + eventTimeString);
            return "";
        }
    }

    public String getDelimiter(String type) {
        String delimiter;
        if (type.equals(StringConstans.TSV))
            delimiter = "\t";
        else if (type.equals(StringConstans.CSV))
            delimiter = ",";
        else
            delimiter = "\001";
        return delimiter;
    }

    @Override
    public Event intercept(Event event) {
        Map<String, String> headers = event.getHeaders();

        String payload = new String(event.getBody(), StandardCharsets.UTF_8);

        // Parsing
        JSONObject parsedValues = parser.parse(payload);

        while (parsedValues == null) {
            if (dumpData()) {
                logger.info("Dump Data is TRUE. json parsing failed data will go to Trash : " + payload);
                return null;
            }
            refresh();
            parsedValues = parser.parse(payload);
        }

        // Timestamp for Event Time.
        if (useEventtime) {
            String eventTimeString = parsedValues.get(timeColumn).toString();
            String ts = getMillisByEventtime(eventTimeString);
            while (ts.equals("")) {
                if (dumpData()) {
                    logger.info("Dump Data is TRUE. parsing failed data will go to Trash" + payload);
                    return null;
                }
                refresh();
                ts = getMillisByEventtime(eventTimeString);
            }
            headers.put(StringConstans.TIMESTAMP, ts);
        }

        // Custom Parsing Logic
        String afterLogic = parser.logic(parsedValues);
        while (afterLogic.equals("")) {
            if (dumpData()) {
                logger.info("Dump Data is TRUE. logic failed data will go to Trash" + payload);
                return null;
            }
            logger.error("Error occur in middle of parsing logic \n" + payload);
            refresh();
            afterLogic = parser.logic(parsedValues);
        }

        event.setBody(afterLogic.getBytes());
        return event;
    }


    public void refresh() {
        // sleep for new metadata
        logger.error("Please check your " + spreadsheetID + "," + sheetName + " spreadsheet and your data that is stored in Kafka");

        logger.error("Wait 10 sec for applying new metadata.");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        logger.error("Refresh Metadata.");
        // Refresh metadata
        loadMetadata();
    }

    @Override
    public List<Event> intercept(List<Event> events) {
        List<Event> failed = new ArrayList<>();
        for (Event event : events) {
            if (intercept(event) == null) {
                failed.add(event);
            }
        }
        events.removeAll(failed);
        return events;
    }

    @Override
    public void close() {
    }

    public static class Builder implements Interceptor.Builder {
        private String spreadsheetID = "";
        private String sheetName = "";
        private boolean useEventtime = false;
        private String timestampFieldName = "";
        private String timeFormat = "";

        @Override
        public void configure(Context context) {
            spreadsheetID = context.getString(StringConstans.SPREADSHEET_ID, "");
            sheetName = context.getString(StringConstans.SHEET_NAME, "");
            useEventtime = context.getBoolean(StringConstans.USE_EVENTTIME, false);
            if (useEventtime) {
                timestampFieldName = context.getString(StringConstans.TIME_COLUMN, "0");
                timeFormat = context.getString(StringConstans.TIME_FORMAT, "yyyy-MM-dd HH:mm:ss");
            }
        }

        @Override
        public Interceptor build() {
            return new ParsingInterceptor(spreadsheetID, sheetName, useEventtime, timestampFieldName, timeFormat);
        }
    }
}
