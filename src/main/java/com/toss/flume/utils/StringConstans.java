package com.toss.flume.utils;

public class StringConstans {
    // Header key
    public final static String TIMESTAMP = "timestamp";

    // Parameters
    public final static String SPREADSHEET_ID = "spreadsheetID";
    public final static String SHEET_NAME = "sheetName";
    public final static String TIME_COLUMN = "timeColumn";
    public final static String TIME_FORMAT = "timeFormat";
    public final static String USE_EVENTTIME = "useEventtime";

    // formats
    public final static String JSON = "json";
    public final static String TSV = "tsv";
    public final static String CSV = "csv";


    public final static String SPREADSHEET_URL= "http://172.30.71.228:5000/api/v1/sheet";

    public final static String EMPTY_STRING="";

}
