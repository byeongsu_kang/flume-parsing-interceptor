import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.junit.Test;

public class JsonTest {
    ObjectMapper mapper = new ObjectMapper();

    @Test
    public void testt() {
        String payload = "{\"mac\":null,\"af_sub1\":null,\"customer_user_id\":\"9433519\",\"bundle_id\":\"viva.republica.toss\",\"af_cost_value\":null,\"app_version\":\"4.14.1-0510T2052-e9dd68f\",\"city\":\"Punggi-Dong\",\"fb_campaign_id\":null,\"device_model\":\"LGM-G600K\",\"af_cost_model\":null,\"af_c_id\":\"1873249664\",\"selected_currency\":\"USD\",\"app_name\":\"토스\",\"install_time_selected_timezone\":\"2019-05-15 19:33:01.726+0900\",\"wifi\":false,\"install_time\":\"2019-05-15 10:33:01\",\"operator\":\"\",\"fb_adgroup_id\":null,\"currency\":\"USD\",\"attributed_touch_type\":\"click\",\"af_adset_id\":\"\",\"re_targeting_conversion_type\":\"re-engagement\",\"attributed_touch_time\":\"2019-05-15 08:30:20\",\"click_time_selected_timezone\":\"2019-05-15 17:30:20.815+0900\",\"revenue_in_selected_currency\":null,\"is_retargeting\":true,\"country_code\":\"KR\",\"event_type\":\"in-app-event\",\"appsflyer_device_id\":\"1515236166577-3743512549228745174\",\"af_sub5\":null,\"fb_campaign_name\":null,\"click_url\":null,\"media_source\":\"googleadwords_int\",\"campaign\":\"UACe_ft_performance_04\",\"af_keywords\":null,\"event_value\":\"{}\",\"ip\":\"110.70.54.198\",\"event_time\":\"2019-05-15 10:33:01\",\"click_time\":\"2019-05-15 08:30:20\",\"af_sub4\":null,\"imei\":null,\"fb_adgroup_name\":null,\"af_sub2\":null,\"attribution_type\":\"regular\",\"android_id\":null,\"af_adset\":\"\",\"fb_adset_id\":null,\"af_ad\":\"\",\"agency\":null,\"fb_adset_name\":null,\"cost_per_install\":null,\"af_channel\":\"Youtube\",\"af_cost_currency\":null,\"device_brand\":\"lge\",\"download_time\":\"2018-01-06 10:56:06\",\"af_siteid\":\"YouTubeVideos\",\"language\":\"한국어\",\"app_id\":\"viva.republica.toss\",\"af_ad_type\":\"AppDeepLink\",\"carrier\":\"KT\",\"event_name\":\"appopen\",\"advertising_id\":\"5068b2ac-a2cd-4294-a645-f6782b7f2a8a\",\"os_version\":\"8.0.0\",\"platform\":\"android\",\"af_sub3\":null,\"download_time_selected_timezone\":\"2018-01-06 19:56:06.000+0900\",\"af_ad_id\":\"\",\"sdk_version\":\"v4.8.18\",\"event_time_selected_timezone\":\"2019-05-15 19:33:01.754+0900\",\"log_time\":\"2019-05-15 19:33:06.579\",\"attributed_touch_time_kst\":\"2019-05-15 17:30:20.000\"}";

        JSONObject jsonObject=new JSONObject(payload);

        System.out.println(jsonObject.toString());
    }

    @Test
    public void splitTest() {
        String sp = "|";
        String aa = "asf|afs|asf";
        String[] aaa = aa.split("\\|");
        System.out.println("a" + aaa[0]);

    }

}
